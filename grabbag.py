import os
import subprocess
from threading import Thread

knife_cmd = 'knife data bag'
databag_dir = "%s/databags" % os.environ['HOME']


def get_all_bags():
  """Run a knife query for all data bags on server."""
  os.makedirs(databag_dir)
  print 'Getting all top level databags'
  with open("%s/allbags.txt" % databag_dir, 'w') as myfile:
    myfile.write(
      str(subprocess.check_output("%s list" % knife_cmd, shell=True))
    )


def make_dirs():
  """Create a directory for each databag."""
  if not os.path.exists(databag_dir):
    os.mkdir(databag_dir)

  for bag in read_bag_file():
    if not os.path.exists("%s/%s" % (databag_dir, bag)):
      print "Creating directory %s" % bag
      os.mkdir("%s/%s" % (databag_dir, bag))


def read_bag_file():
  """Read the databag file we have on disk."""
  with open("%s/allbags.txt" % databag_dir, 'r') as myfile:
    return myfile.read().split("\n")


def write_databag(databag, sub_bag):
    """Write the databag to a json file."""
    local_bag_file = "%s/%s/%s.json" % (databag_dir, databag, sub_bag)

    if not os.path.exists(local_bag_file):
      print "Writing %s" % local_bag_file
      with open(local_bag_file, 'w') as local_databag:
        local_databag.write(str(subprocess.check_output("%s show %s %s -Fj" % (knife_cmd, databag, sub_bag), shell=True)))


if __name__ == '__main__':
  get_all_bags()
  make_dirs()

  # Loop through each data bag and write the json files
  for databag in read_bag_file():
    for sub_bag in str(subprocess.check_output("%s show %s" % (knife_cmd, databag), shell=True)).split("\n"):
      t = Thread(target=write_databag, args=(databag, sub_bag))
      t.start()
