# Grabbag
python script to downlod all databags from a chef server

# What?
Download all databags from chef server

# Why?
The knife utility provides `knife download` functionality for roles, cookbooks, environments
but not databags.

I wrote this quickly to mimic that functionality. Is it perfect? No
